package homework.app;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;


public class App {

    public static void main(String[] args) {
        Task4 task4 = new Task4();
        task4.startThreeds();
    }

    static List<Integer> Task1(List<Integer> list){
        return list.stream()
                .map(x -> x * x + 10)
                .filter(num -> ((num % 10 != 5) && (num % 10 != 6)))
                .toList();
    }

    static Map<Integer, Integer> Task2(List<Integer> list){
        return list.stream()
                .collect(Collectors.toMap(k -> k, v -> 1, (v1,v2) -> v1 + 1))
                .entrySet().stream().filter(e -> e.getValue() != 1)
                .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    }

    static class Task3{
        final Integer ARRAY_CAPACITY = 10;
        private BlockingQueue<Integer> productQueue = new ArrayBlockingQueue<Integer>(ARRAY_CAPACITY);

        void runThread(){
            Thread provider = new Provider(ARRAY_CAPACITY);
            Thread consumer = new Consumer();
            provider.start();
            consumer.start();
        }

        class Provider extends Thread{
            Integer arraySize;

            Provider(int size){
                arraySize = size;
            }

            @Override
            public void run() {
                for(int i = 0; i < arraySize; i++) {
                    try {
                        productQueue.put(i + 1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        class Consumer extends Thread{
            @Override
            public void run() {
                while (true) {
                    try {
                        System.out.println(productQueue.take());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    static class Task4{

        static private final Object monitor = new Object();
        static Integer lastNum = 3;

        static void startThreeds(){

            Thread thread1 = new Thread(Task4::printOne);
            Thread thread2 = new Thread(Task4::printTwo);
            Thread thread3 = new Thread(Task4::printThree);
            thread1.start();
            thread2.start();
            thread3.start();

            try {
                thread1.join();
                thread2.join();
                thread3.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }

        static void printOne(){
            for (int i = 0; i < 6; i++) {
                synchronized (monitor) {
                    while (lastNum != 3) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(1);
                    lastNum = 1;
                    monitor.notifyAll();
                }
            }
        }

        static void printTwo(){
            for (int i = 0; i < 6; i++) {
                synchronized (monitor) {
                    while (lastNum != 1) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(2);
                    lastNum = 2;
                    monitor.notifyAll();
                }
            }
        }

        static void printThree() {
            for (int i = 0; i < 6; i++) {
                synchronized (monitor) {
                    while (lastNum != 2) {
                        try {
                            monitor.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    System.out.println(3);
                    lastNum = 3;
                    monitor.notifyAll();
                }
            }
        }
    }

}


